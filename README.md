### Environment Requirements
* If on production, it's recommended that you have a sudo user instead of root to avoid permission issues with Nginx. Here's a [guide](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04) to do that.
* python3-pip python3-dev build-essential libssl1.0-dev libffi-dev python3-setuptools autoconf autogen automake libtool git htop
* pipenv ([Installation](https://pipenv.readthedocs.io/en/latest/install/#pragmatic-installation-of-pipenv)). Recommended installation method is [here](https://stackoverflow.com/a/47898336/5932252).
* pyenv ([Installation](https://github.com/pyenv/pyenv#installation))

### Instructions
* Open Pipfile and edit the python_version that you require for this project. If you do edit the version number, make sure that you have verified that the dependencies you're going to install are compatible with the python version.
* If your environment does not already have the python version you need, install it with pyenv. If you do, optionally you can set the python version inside the project root by running: 
```sh
pyenv local <<python_version>>
```
This makes sure running python within the folder will always refer to the version you chose. if the installation of the python version failed, try installing libffi-dev (```sudo apt install libffi-dev```)
and zlib (```sudo apt install zlib1g``` and ```sudo apt-get install zlib1g-dev```)

* Now to setup your virtualenv and install dependencies
```sh
pipenv install
```
* Activate the shell for you virtual environment
```sh
pipenv shell
```

* To start the Flask server, run the following commands:
```sh
export FLASK_ENV=development
export FLASK_APP=run.py
flask run
```
Optionally, add the ```--host=0.0.0.0``` argument to make application accessible across the network.

Follow instructions from your terminal to verify your app is running. 

Note that in your project you would generally put instance/ folder in your .gitignore.

More flask documentation [here](http://flask.pocoo.org/docs).

### Deploying on production
* For production, you can use a server like [Gunicorn](https://gunicorn.org/). You can read about connecting it with Flask, [here](http://flask.pocoo.org/docs/latest/deploying/wsgi-standalone/#gunicorn).
* It's a good idea to use Nginx as a reverse proxy. It's definitely required if you plan on using SSL certificates. To put your Gunicorn server behind Nginx, follow instructions [here](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04).

### Getting SSL Certs
The process is detailed in the guide above. However, it maybe better if you can do it for one hostname at a time as the certbot challenge seem to have a problem when redirects are involved.
Following is the process that has worked for me, previously:

* Have your DNS A record as example.com [@].
* Get the certificate for example.com
* Change your DNS A record to www.example.com [www]
* Get the certificate for www.example.com
* Add a URL redirect record from example.com to http://www.example.com/